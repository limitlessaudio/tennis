import {
  incrementScore,
  resetScore,
} from './actionTypes';

export const resetScoreAction = () => ({
  type: resetScore,
});

const incrementScoreAction = player => ({
  type: incrementScore,
  player,
});

export const incrementScoreActionCreator = player => (dispatch, getState) => {
  const state = getState();

  return state.tennis.status.winner !== null ? null : dispatch(incrementScoreAction(player));
};
