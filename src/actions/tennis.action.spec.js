import {
  resetScoreAction,
  incrementScoreActionCreator,
} from './tennis.action';
import {
  incrementScore,
  resetScore,
} from './actionTypes';

describe('tennis.action', () => {
  describe('resetScoreAction', () => {
    it('should return action', () => {
      // ASSIGN
      const expected = {
        type: resetScore,
      };

      // ACT
      const actual = resetScoreAction();

      // ASSERT
      expect(actual).toEqual(expected);
    });
  });

  describe('incrementScoreActionCreator', () => {
    it('should return action', () => {
      // ASSIGN
      const expected = {
        type: incrementScore,
        player: 1,
      };

      const player = 1;
      const dispatch = jest.fn(value => value);
      const getState = jest.fn(() => ({
        tennis: {
          status: {
            winner: null,
          },
        },
      }));

      // ACT
      const actual = incrementScoreActionCreator(player)(dispatch, getState);

      // ASSERT
      expect(actual).toEqual(expected);
    });

    it('should return null when winner is set', () => {
      // ASSIGN
      const expected = null;

      const player = 1;
      const dispatch = jest.fn(value => value);
      const getState = jest.fn(() => ({
        tennis: {
          status: {
            winner: 1,
          },
        },
      }));

      // ACT
      const actual = incrementScoreActionCreator(player)(dispatch, getState);

      // ASSERT
      expect(actual).toEqual(expected);
    });
  });
});
