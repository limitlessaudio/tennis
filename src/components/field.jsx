import React from 'react';
import PropTypes from 'prop-types';

// eslint-disable-next-line jsx-a11y/click-events-have-key-events
const Field = ({ onClick }) => <div className="field" onClick={onClick} role="button" tabIndex="0" />;

Field.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default Field;
