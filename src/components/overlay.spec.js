import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Overlay from './overlay';

configure({ adapter: new Adapter() });

describe('Overlay', () => {
  it('renders component', () => {
    // ASSIGN
    const onClick = jest.fn(() => {});
    const winner = 'Test Player';

    // ACT
    const actual = shallow(<Overlay onReset={onClick} winner={winner} />);

    // ASSERT
    expect(actual).toMatchSnapshot();
  });
});
