import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Board from './board';

configure({ adapter: new Adapter() });

describe('Board', () => {
  it('renders component', () => {
    // ASSIGN
    const props = {
      board: [
        {
          playerName: 'Test Player 1',
          score: 1,
        },
        {
          playerName: 'Test Player 2',
          score: 0,
        },
      ],
      status: {
        deuce: false,
      },
    };

    // ACT
    const actual = shallow(<Board {...props} />);

    // ASSERT
    expect(actual).toMatchSnapshot();
  });

  it('renders component when deuce happens', () => {
    // ASSIGN
    const props = {
      board: [
        {
          playerName: 'Test Player 1',
          score: 3,
        },
        {
          playerName: 'Test Player 2',
          score: 3,
        },
      ],
      status: {
        deuce: true,
      },
    };

    // ACT
    const actual = shallow(<Board {...props} />);

    // ASSERT
    expect(actual).toMatchSnapshot();
  });
});
