import React from 'react';
import PropTypes from 'prop-types';

const Overlay = ({ onReset, winner }) => (
  <div className="overlay">
    <p>
      <span className="winner-name">{winner}</span>
      <span> won!</span>
    </p>
    <button onClick={onReset} type="button">New Game</button>
  </div>
);

Overlay.propTypes = {
  onReset: PropTypes.func.isRequired,
  winner: PropTypes.string.isRequired,
};

export default Overlay;
