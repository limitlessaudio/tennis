import React from 'react';
import PropTypes from 'prop-types';
import getDisplayScore from '../utils/getDisplayScore';

const Board = ({ board, status }) => (
  <div className="board">
    <p className="players-title">
      <span>{board[0].playerName}</span>
      <span>
        {getDisplayScore(board[0].score)}
        {' : '}
        {getDisplayScore(board[1].score)}
      </span>
      <span>{board[1].playerName}</span>
    </p>
    <p><small>{status.deuce ? 'DEUCE' : ''}</small></p>
  </div>
);

Board.propTypes = {
  board: PropTypes.arrayOf(
    PropTypes.shape({
      score: PropTypes.number.isRequired,
    }),
  ).isRequired,
  status: PropTypes.shape({
    deuce: PropTypes.bool.isRequired,
  }).isRequired,
};

export default Board;
