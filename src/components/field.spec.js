import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Field from './field';

configure({ adapter: new Adapter() });

describe('Field', () => {
  it('renders component', () => {
    // ASSIGN
    const onClick = jest.fn(() => {});

    // ACT
    const actual = shallow(<Field onClick={onClick} />);

    // ASSERT
    expect(actual).toMatchSnapshot();
  });
});
