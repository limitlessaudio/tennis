/* eslint-disable react/prop-types */
import React from 'react';
import './App.scss';
import { connect } from 'react-redux';
import Board from './components/board';
import Field from './components/field';
import Overlay from './components/overlay';
import {
  incrementScoreActionCreator,
  resetScoreAction,
} from './actions/tennis.action';
import getWinnerName from './utils/getWinnerName';

function App(props) {
  const {
    onClick,
    onReset,
    board,
    status,
  } = props;
  return (
    <div className="App">
      {
        status.winner !== null &&
        <Overlay onReset={() => onReset()} winner={getWinnerName(board, status)} />
      }
      <header>
        <Board board={board} status={status} />
      </header>
      <section className="court">
        {
          [0, 1].map((item, index) => (
            <Field key={item} onClick={() => onClick(index)} />
          ))
        }
      </section>
    </div>
  );
}

const mapStateToProps = state => ({
  board: state.tennis.board,
  status: state.tennis.status,
});

const mapDispatchToProps = dispatch => ({
  onClick: player => dispatch(incrementScoreActionCreator(player)),
  onReset: () => dispatch(resetScoreAction()),
});

export const AppToTest = App;

export default connect(mapStateToProps, mapDispatchToProps)(App);
