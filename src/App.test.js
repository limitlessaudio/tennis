import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { AppToTest } from './App';

configure({ adapter: new Adapter() });

jest.mock('./components/overlay', () => 'MockOverlay');
jest.mock('./components/board', () => 'MockBoard');
jest.mock('./components/field', () => 'MockField');

describe('App', () => {
  describe('when in game', () => {
    it('renders component', () => {
      // ASSIGN
      const props = {
        board: [
          {
            playerName: 'Test Player 1',
            score: 1,
          },
          {
            playerName: 'Test Player 2',
            score: 0,
          },
        ],
        status: {
          deuce: false,
          winner: null,
        },
      };

      // ACT
      const actual = shallow(<AppToTest {...props} />);

      // ASSERT
      expect(actual).toMatchSnapshot();
    });
  });
  describe('when game ended', () => {
    it('renders component', () => {
      // ASSIGN
      const props = {
        board: [
          {
            playerName: 'Test Player 1',
            score: 4,
          },
          {
            playerName: 'Test Player 2',
            score: 0,
          },
        ],
        status: {
          deuce: false,
          winner: 0,
        },
      };

      // ACT
      const actual = shallow(<AppToTest {...props} />);

      // ASSERT
      expect(actual).toMatchSnapshot();
    });
  });
});
