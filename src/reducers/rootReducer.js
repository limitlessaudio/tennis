import { combineReducers } from 'redux';
import tennisReducer from './tennis.reducer';

export default combineReducers({
  tennis: tennisReducer,
});
