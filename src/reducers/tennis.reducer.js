import {
  incrementScore,
  resetScore,
} from '../actions/actionTypes';
import {
  isDeuce,
  isScore,
  handleIncrementScore,
  hasPointAdvantage,
  getWinner,
} from '../utils/score-utils';

const initialState = {
  status: {
    deuce: false,
    pointAdvantage: false,
    winner: null,
  },
  board: [
    {
      playerName: 'Alice',
      score: 0,
    },
    {
      playerName: 'Bob',
      score: 0,
    },
  ],
};

const tennisReducer = (state = initialState, action) => {
  switch (action.type) {
  case incrementScore: {
    const newBoard = handleIncrementScore(state.board, action.player);

    return {
      ...state,
      status: {
        ...state.status,
        pointAdvantage: hasPointAdvantage(newBoard),
        deuce: isDeuce(newBoard),
        winner: isScore(newBoard) ? getWinner(newBoard) : null,
      },
      board: newBoard,
    };
  }
  case resetScore:
    return initialState;

  default:
    return state;
  }
};

export default tennisReducer;
