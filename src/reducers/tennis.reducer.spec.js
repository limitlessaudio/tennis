import tennisReducer from './tennis.reducer';
import {
  incrementScore,
  resetScore,
} from '../actions/actionTypes';

describe('tennis.reducer', () => {
  describe('state is unset', () => {
    describe('action is irrelevant', () => {
      it('should return initial state', () => {
        // ASSIGN
        const expected = {
          status: {
            deuce: false,
            pointAdvantage: false,
            winner: null,
          },
          board: [
            {
              playerName: 'Alice',
              score: 0,
            },
            {
              playerName: 'Bob',
              score: 0,
            },
          ],
        };
        const state = undefined;
        const action = {
          type: 'test-action',
        };

        // ACT
        const actual = tennisReducer(state, action);

        // ASSERT
        expect(actual).toEqual(expected);
      });
    });
    describe('incrementScore', () => {
      it('should return new state', () => {
        // ASSIGN
        const expected = {
          status: {
            deuce: false,
            pointAdvantage: false,
            winner: null,
          },
          board: [
            {
              playerName: 'Alice',
              score: 1,
            },
            {
              playerName: 'Bob',
              score: 0,
            },
          ],
        };
        const state = undefined;
        const action = {
          type: incrementScore,
          player: 0,
        };

        // ACT
        const actual = tennisReducer(state, action);

        // ASSERT
        expect(actual).toEqual(expected);
      });
    });
  });

  describe('state is set', () => {
    describe('resetScore', () => {
      it('should return initial state', () => {
        // ASSIGN
        const expected = {
          status: {
            deuce: false,
            pointAdvantage: false,
            winner: null,
          },
          board: [
            {
              playerName: 'Alice',
              score: 0,
            },
            {
              playerName: 'Bob',
              score: 0,
            },
          ],
        };
        const state = {
          foo: 'bar',
        };
        const action = {
          type: resetScore,
        };

        // ACT
        const actual = tennisReducer(state, action);

        // ASSERT
        expect(actual).toEqual(expected);
      });
    });
    describe('incrementScore', () => {
      it('should return new state', () => {
        // ASSIGN
        const expected = {
          status: {
            deuce: false,
            pointAdvantage: true,
            winner: 0,
          },
          board: [
            {
              playerName: 'Alice',
              score: 4,
            },
            {
              playerName: 'Bob',
              score: 0,
            },
          ],
        };
        const state = {
          status: {
            deuce: false,
            pointAdvantage: false,
            winner: null,
          },
          board: [
            {
              playerName: 'Alice',
              score: 3,
            },
            {
              playerName: 'Bob',
              score: 0,
            },
          ],
        };
        const action = {
          type: incrementScore,
          player: 0,
        };

        // ACT
        const actual = tennisReducer(state, action);

        // ASSERT
        expect(actual).toEqual(expected);
      });
    });
  });
});
