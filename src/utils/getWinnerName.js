const isValidBoard = board => Array.isArray(board) && board.length === 2 &&
board.every(item => !!item.playerName);
const isValidStatus = status => !!status && [0, 1].includes(status.winner);

const getWinnerName = (board, status) => (isValidBoard(board) && isValidStatus(status) ?
  board[status.winner].playerName :
  '');

export default getWinnerName;
