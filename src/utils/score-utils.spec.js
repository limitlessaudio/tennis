import {
  hasPointAdvantage,
  isDeuce,
  isScore,
  getWinner,
  handleIncrementScore,
} from './score-utils';

describe('score-utils', () => {
  describe('hasPointAdvantage', () => {
    it('should return true when difference is more than 2 points', () => {
      // ASSIGN
      const expected = true;
      const board = [
        {
          score: 0,
        },
        {
          score: 2,
        },
      ];

      // ACT
      const actual = hasPointAdvantage(board);

      // ASSERT
      expect(actual).toEqual(expected);
    });

    it('should return false when difference is more less 2 points', () => {
      // ASSIGN
      const expected = false;
      const board = [
        {
          score: 0,
        },
        {
          score: 1,
        },
      ];

      // ACT
      const actual = hasPointAdvantage(board);

      // ASSERT
      expect(actual).toEqual(expected);
    });
  });

  describe('isDeuce', () => {
    it('should return true when points are 3 : 3', () => {
      // ASSIGN
      const expected = true;
      const board = [
        {
          score: 3,
        },
        {
          score: 3,
        },
      ];

      // ACT
      const actual = isDeuce(board);

      // ASSERT
      expect(actual).toEqual(expected);
    });
    it('should return true when points are 4 : 4', () => {
      // ASSIGN
      const expected = true;
      const board = [
        {
          score: 4,
        },
        {
          score: 4,
        },
      ];

      // ACT
      const actual = isDeuce(board);

      // ASSERT
      expect(actual).toEqual(expected);
    });
    it('should return false when points are less than 3', () => {
      // ASSIGN
      const expected = false;
      const board = [
        {
          score: 2,
        },
        {
          score: 3,
        },
      ];

      // ACT
      const actual = isDeuce(board);

      // ASSERT
      expect(actual).toEqual(expected);
    });

    it('should return false when points are more than 2 but not equal', () => {
      // ASSIGN
      const expected = false;
      const board = [
        {
          score: 4,
        },
        {
          score: 3,
        },
      ];

      // ACT
      const actual = isDeuce(board);

      // ASSERT
      expect(actual).toEqual(expected);
    });
  });

  describe('isScore', () => {
    it('should return true when points are 4 : 2', () => {
      // ASSIGN
      const expected = true;
      const board = [
        {
          score: 4,
        },
        {
          score: 2,
        },
      ];

      // ACT
      const actual = isScore(board);

      // ASSERT
      expect(actual).toEqual(expected);
    });
    it('should return true when points are 5 : 3', () => {
      // ASSIGN
      const expected = true;
      const board = [
        {
          score: 5,
        },
        {
          score: 3,
        },
      ];

      // ACT
      const actual = isScore(board);

      // ASSERT
      expect(actual).toEqual(expected);
    });
    it('should return false when points are less than 4', () => {
      // ASSIGN
      const expected = false;
      const board = [
        {
          score: 3,
        },
        {
          score: 3,
        },
      ];

      // ACT
      const actual = isScore(board);

      // ASSERT
      expect(actual).toEqual(expected);
    });

    it('should return false when points are more than 3 but difference if less than 2 points', () => {
      // ASSIGN
      const expected = false;
      const board = [
        {
          score: 4,
        },
        {
          score: 5,
        },
      ];

      // ACT
      const actual = isScore(board);

      // ASSERT
      expect(actual).toEqual(expected);
    });
  });

  describe('getWinner', () => {
    it('should return 0 when first score is higher', () => {
      // ASSIGN
      const expected = 0;
      const board = [
        {
          score: 1,
        },
        {
          score: 0,
        },
      ];

      // ACT
      const actual = getWinner(board);

      // ASSERT
      expect(actual).toEqual(expected);
    });

    it('should return 1 when second score is higher', () => {
      // ASSIGN
      const expected = 1;
      const board = [
        {
          score: 0,
        },
        {
          score: 1,
        },
      ];

      // ACT
      const actual = getWinner(board);

      // ASSERT
      expect(actual).toEqual(expected);
    });
  });

  describe('handleIncrementScore', () => {
    it('should return expected new array', () => {
      // ASSIGN
      const expected = [
        {
          foo: 'bar',
          score: 0,
        },
        {
          bar: 'baz',
          score: 1,
        },
      ];
      const index = 1;
      const board = [
        {
          foo: 'bar',
          score: 0,
        },
        {
          bar: 'baz',
          score: 0,
        },
      ];

      // ACT
      const actual = handleIncrementScore(board, index);

      // ASSERT
      expect(actual).toEqual(expected);
    });
  });
});
