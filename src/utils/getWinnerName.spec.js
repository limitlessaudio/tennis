import getWinnerName from './getWinnerName';

describe('getWinnerName', () => {
  it('should return winner name', () => {
    // ASSIGN
    const expected = 'Test player 1';
    const board = [
      {
        playerName: 'Test player 1',
      },
      {
        playerName: 'Test player 2',
      },
    ];
    const status = {
      winner: 0,
    };

    // ACT
    const actual = getWinnerName(board, status);

    // ASSERT
    expect(actual).toBe(expected);
  });

  it('should return empty name', () => {
    // ASSIGN
    const expected = '';
    const board = null;
    const status = null;

    // ACT
    const actual = getWinnerName(board, status);

    // ASSERT
    expect(actual).toBe(expected);
  });
});
