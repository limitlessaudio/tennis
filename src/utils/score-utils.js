export const hasPointAdvantage = board => Math.abs(board[0].score - board[1].score) > 1;

const isScoreGreaterThan = (board, score) => board.some(item => item.score > score);
export const isDeuce = board => isScoreGreaterThan(board, 2) && board[0].score === board[1].score;
export const isScore = board => isScoreGreaterThan(board, 3) && hasPointAdvantage(board);

export const getWinner = board => (board[0].score > board[1].score ? 0 : 1);

export const handleIncrementScore = (board, index) => board.map(
  (player, playerIndex) => (playerIndex === index ?
    {
      ...player,
      score: player.score + 1,
    } :
    player
  ),
);
