import getDisplayScore from './getDisplayScore';

describe('getDisplayScore', () => {
  it('should return 0 if input is 0', () => {
    // ASSIGN
    const expected = 0;
    const score = 0;

    // ACT
    const actual = getDisplayScore(score);

    // ASSERT
    expect(actual).toEqual(expected);
  });

  it('should return 15 if input is 1', () => {
    // ASSIGN
    const expected = 15;
    const score = 1;

    // ACT
    const actual = getDisplayScore(score);

    // ASSERT
    expect(actual).toEqual(expected);
  });

  it('should return 30 if input is 2', () => {
    // ASSIGN
    const expected = 30;
    const score = 2;

    // ACT
    const actual = getDisplayScore(score);

    // ASSERT
    expect(actual).toEqual(expected);
  });

  it('should return 40 if input is 3', () => {
    // ASSIGN
    const expected = 40;
    const score = 3;

    // ACT
    const actual = getDisplayScore(score);

    // ASSERT
    expect(actual).toEqual(expected);
  });

  it('should return 40 if input is greater than 3', () => {
    // ASSIGN
    const expected = 40;
    const score = 5;

    // ACT
    const actual = getDisplayScore(score);

    // ASSERT
    expect(actual).toEqual(expected);
  });
});
