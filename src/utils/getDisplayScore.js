const displayScore = {
  0: 0,
  1: 15,
  2: 30,
  3: 40,
};

const getDisplayScore = score => (score < 4 ? displayScore[score] : displayScore[3]);

export default getDisplayScore;
